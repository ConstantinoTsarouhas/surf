// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **dynamic optional value** represents a value that might be unavailable (`nil`, `NULL` …) in a dynamic context.
public final class DynamicOptional<Element : DynamicType> : DynamicType {
	
	// TODO
	
	/// Registers an availability bind statement in a concoction.
	///
	/// - Parameter 1: The concoction if the value is available, with as sole argument that value.
	///
	/// - Returns: A remainder object for optionally registering an alternative clause. It may be discarded if not needed.
	public func ifAvailable(concoction: Element -> ()) -> AvailabilityBindStatementRemainder {
		return AvailabilityBindStatementRemainder()		// TODO
	}
	
}

public final class AvailabilityBindStatementRemainder {
	
	private init() {
		// TODO
	}
	
	/// Registers an alternative clause if the dynamic optional value were not to be available.
	///
	/// - Warning: Any registration must be done immediately following the availability guard statement and must be done exactly once. The compiler raises an error if done otherwise.
	public func otherwise(sourceFilename sourceFilename: StaticString = __FILE__, sourceLineNumber: Int = __LINE__, _ concoction: () -> ()) {
		// TODO
	}
	
}