// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **dynamic function** represents a set of operations in a dynamic environment that produce a result. Functions do not modify the environment.
public final class DynamicFunction<DynamicArgument : DynamicType, DynamicResult : DynamicType> : DynamicType {
	
	/// Initialises the dynamic function.
	///
	/// - Parameter 1: The concoctor, which must follow the concoction rules for functions. Surf will execute the concoctor during generation and determine which operations are to be serialised into executable code.
	public init(_ concoctor: Concoctor) {
		self.concoctor = concoctor
	}
	
	private let concoctor: Concoctor
	public typealias Concoctor = DynamicArgument -> DynamicResult
	
}