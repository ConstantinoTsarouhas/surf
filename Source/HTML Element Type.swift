// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

public enum HTMLElementType {
	
	case Document
	case DocumentHeader
	case DocumentBody
	
	case Span
	case Division
	case Section
	
	case ListItem
	case Paragraph
	
	// TODO
	
}

extension HTMLElementType : QualifiedXMLTypeRepresentable {
	
	public init?(qualifiedXMLType: QualifiedXMLType) {
		return nil									// TODO
	}
	
	public var qualifiedXMLType: QualifiedXMLType {
		return QualifiedXMLType(identifier: "")		// TODO
	}
	
}