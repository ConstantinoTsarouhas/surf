// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import Foundation

/// An **XML element** value represents a simplified XML element, i.e., an element's type, attributes, and children.
///
/// `XMLElement` provides no support for document type declarations or schemata, but it does provides XML namespace support. In addition, `XMLElement` prefers a simpler representation in place of a detailed one: some information such as order of attributes or the unparsed state of CDATA sections are not preserved.
public struct XMLElement {
	
	/// Initialises the XML element.
	public init(type: QualifiedXMLType, attributes: AttributeSet = [:], children: [Child] = []) {
		self.type = type
		self.attributes = attributes
		self.children = children
	}
	
	public init(data: NSData, validate: Bool = true) throws {
		self.init(type: QualifiedXMLType(identifier: "", namespace: nil))	// TODO
	}
	
	/// The element's type.
	public var type: QualifiedXMLType
	
	/// The element's attributes.
	public var attributes: AttributeSet
	public typealias AttributeSet = [QualifiedXMLType : String]
	
	/// The element's children.
	public var children: [Child]
	public enum Child {
		
		/// The child is an element.
		///
		/// - Parameter 1: The element.
		case Element(XMLElement)
		
		/// The child is a textual sequence. Both parsed and unparsed character data are represented as textual sequences, without distinction between the two forms. During generation, textual sequences are represented as parsed character data.
		///
		/// - Parameter 1: The textual sequence.
		case Text(String)
		
		/// The child is a processing instruction.
		///
		/// - Parameter target: The target.
		/// - Parameter value: The processing instruction.
		case ProcessingInstruction(target: String, value: String)
		
		/// The child is a comment.
		///
		/// - Parameter 1: The comment.
		case Comment(String)
		
	}
	
	/// The element's child elements.
	public var childElements: [XMLElement] {
		var result = [XMLElement]()
		for child in children {
			switch child {
				case .Element(let e):	result.append(e)
				default:				continue
			}
		}
		return result
	}
	
}

extension XMLElement : Hashable {
	public var hashValue: Int {
		return 0	// TODO
	}
}

public func ==(l: XMLElement, r: XMLElement) -> Bool {
	return l.type == r.type && l.attributes == r.attributes && l.children == r.children
}

extension XMLElement.Child : Hashable {
	public var hashValue: Int {
		switch self {
			case .Element(let e):						return e.hashValue
			case .Text(let s):							return s.hashValue &+ 1
			case .ProcessingInstruction(let t, let i):	return t.hashValue &+ i.hashValue &* 31
			case .Comment(let s):						return s.hashValue &+ 2
		}
	}
}

public func ==(l: XMLElement.Child, r: XMLElement.Child) -> Bool {
	switch (l, r) {
		case (.Element(let e1), .Element(let e2)):												return e1 == e2
		case (.Text(let s1), .Text(let s2)):													return s1 == s2
		case (.ProcessingInstruction(let t1, let i1), .ProcessingInstruction(let t2, let i2)):	return t1 == t2 && i1 == i2
		case (.Comment(let s1), .Comment(let s2)):												return s1 == s2
		default:																				return false
	}
}