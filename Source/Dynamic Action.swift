// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **dynamic action** represents a set of operations in a dynamic environment that modify that environment.
public final class DynamicAction<Environment : DynamicEnvironmentType> : DynamicType {
	
	/// Initialises the dynamic action.
	///
	/// - Parameter 1: The concoctor, which must follow the concoction rules for actions. Surf will execute the concoctor during generation and determine which operations are to be serialised into executable code.
	public init(_ concoctor: Concoctor) {
		self.concoctor = concoctor
	}
	
	private let concoctor: Concoctor
	public typealias Concoctor = Environment -> ()
	
}

public enum ConcoctingError : ErrorType {
	
	/// The availability guard statement remainder object has been used outside its correct context, which is immediately after the guard statement that generates the remainder. Since a second or subsequent use of the remainder object cannot be immediately after the guard statement, it equally can result in this error.
	///
	/// - Parameter sourceFilename: The filename of the Swift source where the remainder object has been used in an illegal manner.
	/// - Parameter sourceLineNumber: The line number of the Swift source where the remainder object has been used in an illegal manner.
	case UseOfAvailabilityGuardStatementRemainderOutsideOfCorrectContext(sourceFilename: StaticString, sourceLineNumber: Int)
	
	// TODO
	
}