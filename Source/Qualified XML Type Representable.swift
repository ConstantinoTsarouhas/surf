// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// Instances of **qualified XML type representable** types can be represented by qualified XML types.
public protocol QualifiedXMLTypeRepresentable {
	
	/// Initialises the representable with given qualified XML type.
	init?(qualifiedXMLType: QualifiedXMLType)
	
	/// The qualified XML type represented by the representable.
	var qualifiedXMLType: QualifiedXMLType { get }
	
}