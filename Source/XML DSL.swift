// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

prefix operator <• {}
infix operator • { precedence 137 associativity left }
infix operator •> { precedence 137 associativity left }

public prefix func <•(type: QualifiedXMLType) -> XMLElement {
	return XMLElement(type: type)
}

public func •(var element: XMLElement, attributes: XMLElement.AttributeSet) -> XMLElement {
	element.attributes = attributes		// TODO: Optionally append instead.
	return element
}

public func •>(var element: XMLElement, children: [XMLElement.Child]) -> XMLElement.Child {
	element.children += children
	return .Element(element)
}

extension XMLElement.Child : StringLiteralConvertible {
	
	public init(extendedGraphemeClusterLiteral value: String) {
		self = .Text(value)
	}
	
	public init(stringLiteral value: String) {
		self = .Text(value)
	}
	
	public init(unicodeScalarLiteral value: String) {
		self = .Text(value)
	}

}

public protocol XMLElementChildRepresentable {
	var elementChildRepresentation: XMLElement.Child { get }
}

extension String : XMLElementChildRepresentable {
	public var elementChildRepresentation: XMLElement.Child {
		return .Text(self)
	}
}

extension XMLElement : XMLElementChildRepresentable {
	public var elementChildRepresentation: XMLElement.Child {
		return .Element(self)
	}
}