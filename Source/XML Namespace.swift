// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import Foundation

/// An **XML namespace** groups element and attribute type identifiers so that they don't conflict with identifiers in other namespaces.
public struct XMLNamespace {
	
	/// Initialises the namespace.
	///
	/// - Requires: The identifier is valid.
	public init(identifier: String) {
//		precondition(XMLNamespace.isValidIdentifier(identifier), "The identifier must be valid.")
		self.identifier = identifier
	}
	
	/// The namespace's identifier; typically a URI.
	///
	/// - Invariant: The identifier is valid.
	public var identifier: String {
		willSet {
//			precondition(XMLNamespace.isValidIdentifier(newValue), "The identifier must be valid.")
		}
	}
	
	/// The regular expression that matches valid namespace identifiers only.
	private static let identifierValidityRegularExpression = try! NSRegularExpression(pattern: "", options: [])		// TODO: Add pattern.
	
	/// Determines whether given namespace identifier is valid.
	///
	/// - Parameter identifier: The identifier to validate.
	///
	/// - Returns: Whether given identifier is valid.
	public static func isValidIdentifier(identifier: String) -> Bool {
		return identifierValidityRegularExpression.matchesInString(identifier, options: [], range: NSRange.init(location: 0, length: (identifier as NSString).length)).count == 1
	}
	
	/// Generates qualified XML type belonging to the namespace.
	public subscript (typeIdentifier: String) -> QualifiedXMLType {
		return QualifiedXMLType(identifier: typeIdentifier, namespace: self)
	}
	
}

extension XMLNamespace : Hashable {
	public var hashValue: Int {
		return identifier.hashValue
	}
}

public func ==(l: XMLNamespace, r: XMLNamespace) -> Bool {
	return l.identifier == r.identifier
}