// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// A **dynamic element** represents an element in a dynamic Document Object Model context.
public final class DynamicElement : DynamicType {
	
	/// Initialises a new dynamic element with given qualified XML type.
	///
	/// This initialiser supports use in a concoction.
	///
	/// - Parameter type: A value that represents the qualified XML type for the new dyanmic element.
	public init(type: QualifiedXMLTypeRepresentable) {
		// TODO
	}
	
}