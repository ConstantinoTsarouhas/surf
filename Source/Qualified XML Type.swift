// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import Foundation

/// A **qualified XML type** identifies the type of an XML element or attribute.
public struct QualifiedXMLType {
	
	/// Initialises the identifier.
	///
	/// - Requires: The identifier is valid.
	public init(identifier: String, namespace: XMLNamespace? = nil) {
//		precondition(QualifiedXMLType.isValidIdentifier(identifier), "The identifier must be valid.")
		self.identifier = identifier
		self.namespace = namespace
	}
	
	/// The qualified type's identifier.
	///
	/// - Invariant: The identifier is valid.
	public var identifier: String {
		willSet {
//			precondition(XMLNamespace.isValidIdentifier(newValue), "The identifier must be valid.")
		}
	}
	
	/// The regular expression that matches valid qualified type identifiers only.
	private static let identifierValidityRegularExpression = try! NSRegularExpression(pattern: "", options: [])		// TODO: Add pattern.
	
	/// Determines whether given qualified type identifier is valid.
	///
	/// - Parameter identifier: The identifier to validate.
	///
	/// - Returns: Whether given identifier is valid.
	public static func isValidIdentifier(identifier: String) -> Bool {
		return identifierValidityRegularExpression.matchesInString(identifier, options: [], range: NSRange.init(location: 0, length: (identifier as NSString).length)).count == 1
	}
	
	/// The qualified type's namespace, or nil if belongs to the default namespace.
	public var namespace: XMLNamespace?
	
}

extension QualifiedXMLType : Hashable {
	public var hashValue: Int {
		return identifier.hashValue &+ (namespace?.hashValue ?? 0)
	}
}

public func ==(l: QualifiedXMLType, r: QualifiedXMLType) -> Bool {
	return l.identifier == r.identifier && l.namespace == r.namespace
}