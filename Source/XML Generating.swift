// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

import Foundation

public extension XMLElement {
	
	/// Represents the element as a root element in a binary–serialised document.
	///
	/// - Parameter preficesByNamespace: The prefices to use by namespace, declared at root level.
	///
	/// - Requires: The prefices are valid XML namespace prefices.
	public func serialiseAsRootElementInDocument(preficesByNamespace preficesByNamespace: [XMLNamespace : String]) throws -> NSData {
		return try documentRepresentation(preficesByNamespace: preficesByNamespace).XMLDataWithOptions(NSXMLNodePrettyPrint)
	}
	
	/// Represents the element as a root element in a string–serialised document.
	///
	/// - Parameter preficesByNamespace: The prefices to use by namespace, declared at root level.
	///
	/// - Requires: The prefices are valid XML namespace prefices.
	public func serialiseAsRootElementInDocument(preficesByNamespace preficesByNamespace: [XMLNamespace : String]) throws -> NSString {
		return try documentRepresentation(preficesByNamespace: preficesByNamespace).XMLStringWithOptions(NSXMLNodePrettyPrint)
	}
	
	private func documentRepresentation(preficesByNamespace preficesByNamespace: [XMLNamespace : String]) throws -> NSXMLDocument {
		
		let namespaceNodes = preficesByNamespace.map { (identifier, prefix) in
			NSXMLNode.namespaceWithName(prefix, stringValue: identifier.identifier) as! NSXMLNode
		}
		
		let rootElement = try elementRepresentation(preficesByNamespace: preficesByNamespace)
		rootElement.namespaces = namespaceNodes
		
		return NSXMLDocument(rootElement: rootElement)
		
	}
	
	private func elementRepresentation(preficesByNamespace preficesByNamespace: [XMLNamespace : String]) throws -> NSXMLElement {
		
		func fullNameFromQualifiedType(type: QualifiedXMLType) throws -> String {
			if let namespace = type.namespace {
				guard let prefix = preficesByNamespace[namespace] else { throw SerialisationError.NoPrefixForNamespace(namespace) }
				return "\(prefix):\(type.identifier)"
			} else {
				return type.identifier
			}
		}
		
		let element = NSXMLElement(name: try fullNameFromQualifiedType(type), URI: type.namespace?.identifier)
		
		let attributeNodes = try attributes.map { type, value -> NSXMLNode in
			if let namespace = type.namespace {
				return NSXMLNode.attributeWithName(try fullNameFromQualifiedType(type), URI: namespace.identifier, stringValue: value) as! NSXMLNode
			} else {
				return NSXMLNode.attributeWithName(type.identifier, stringValue: value) as! NSXMLNode
			}
		}
		
		for attribute in attributeNodes {
			element.addAttribute(attribute)
		}
		
		element.setChildren(try children.map { child -> NSXMLNode in
			switch child {
				case .Element(let e):						return try e.elementRepresentation(preficesByNamespace: preficesByNamespace)
				case .Text(let s):							return NSXMLNode.textWithStringValue(s) as! NSXMLNode
				case .ProcessingInstruction(let n, let v):	return NSXMLNode.processingInstructionWithName(n, stringValue: v) as! NSXMLNode
				case .Comment(let s):						return NSXMLNode.commentWithStringValue(s) as! NSXMLNode
			}
		})
		
		return element
		
	}
	
	public enum SerialisationError : ErrorType {
		
		/// No prefix for a namespace has been declared.
		///
		/// - Parameter 1: The namespace for which no prefix hasn't declared.
		case NoPrefixForNamespace(XMLNamespace)
		
	}
	
}