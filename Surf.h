// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

@import Foundation;

FOUNDATION_EXPORT double SurfVersionNumber;
FOUNDATION_EXPORT const unsigned char SurfVersionString[];
