// Surf © 2016 Constantino Tsarouhas
// See LICENSE file in project for licensing information.

/// Registers a boolean–expression guard statement in a concoction.
///
/// - Parameter 1: The dynamic boolean to ensure being true.
/// - Parameter otherwise: The concoction if the boolean is false. The operations herein are executed instead of the actions that follow the guard statement.
public func ensure(booleanValue: DynamicBoolean, otherwise: () -> () = { _ in }) {
	// TODO
}

/// Registers an availability guard statement in a concoction.
///
/// - Parameter 1: The dynamic optional value to ensure being available.
/// - Parameter available: The concoction if the value is available, with as sole argument that value.
///
/// - Returns: A remainder object for optionally registering an alternative clause. It may be discarded if not needed.
public func ensureAvailable<T>(optionalValue: DynamicOptional<T>, available: T -> () = { _ in }) -> AvailabilityGuardStatementRemainder {
	return AvailabilityGuardStatementRemainder()	// TODO
}

public final class AvailabilityGuardStatementRemainder {
	
	private init() {
		// TODO
	}
	
	/// Registers an alternative clause if the dynamic optional value were not to be available.
	///
	/// - Parameter sourceFilename: The Swift source's filename in where this method is being called. This argument should be defaulted.
	/// - Parameter sourceLineNumber: The Swift source's line number in where this method is being called. This argument should be defaulted.
	/// - Parameter 3: The concoction for the alternative clause.
	///
	/// - Warning: Any registration must be done immediately following the availability guard statement and must be done exactly once. The compiler raises an error if done otherwise.
	public func otherwise(sourceFilename sourceFilename: StaticString = __FILE__, sourceLineNumber: Int = __LINE__, _ concoction: () -> ()) {
		// TODO
	}
	
}

/// Registers a graceful termination statement in a concoction. No operations in the current action are performed after termination and the system resumes any higher-level actions.
public func terminateGracefully() {
	// TODO
}

/// Registers a fatal termination statement in a concoction. No operations in the current action are performed after termination and the system resumes any higher-level actions.
///
/// - Parameter sourceFilename: The Swift source's filename in where this method is being called. This argument should be defaulted.
/// - Parameter sourceLineNumber: The Swift source's line number in where this method is being called. This argument should be defaulted.
/// - Parameter reason: The reason for the fatal termination, if desired.
public func terminateFatally(sourceFilename sourceFilename: StaticString = __FILE__, sourceLineNumber: Int = __LINE__, reason: String? = nil) {
	// TODO
}

// TODO: Error handling-enabled termination.