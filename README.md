# The Surf library
**Surf** is a library for generating HTML, XML, CSS, and JavaScript serialisations of documents, stylesheets, and scripts. It prefers ease-of-use above expressivity, so some features are intentionally unavailable.

Surf does *not* provide an HTTP server, nor is it a web application framework. The *Surf Instant* library combines Surf functionality with HTTP functionality from the Serve library to provide a ready-to-use and fully Swift-based webserver while the *Stack* library provides an abstraction over Surf with more semantic HTML, XML, CSS, and JavaScript generation and integrated HTTP server functionality.

Surf is currently a work-in-progress. One thing that works a little bit is the XML aspect. Surf even includes a domain-specific language within Swift for extra shenanigans.

## Extensible Markup Language (XML)
XML is a verbose and generic language for documents. Surf can parse (read) and generate (write) XML, and it includes support for XML Namespaces.

### Parsing & Manipulating
To parse some binary data containing XML, use `XMLElement`'s initialiser for parsing.

	let data = NSData(…)
	let rootElement = try XMLElement(data: data)

`XMLElement` is a value type and can be manipulated at will using the `attributes`, `children`, and `childElements` properties.

The `attributes` property is a dictionary mapping qualified attribute types to their values in that element. For example, the `href` attribute of an HTML `a` element can be fetched or modified as follows:

	let namespaceForHTML = XMLNamespace(identifier: "http://www.w3.org/1999/xhtml")
	let href = namespaceForHTML["href"]
	
	let anchorElement: XMLElement = …
	
	// Let's get the URL the anchor points to.
	let URL = anchorElement.attributes[href]
	
	// Let's make a new anchor element pointing to another URL.
	var newAnchorElement = anchorElement
	newAnchorElement.attributes[href] = "http://example.com./"

The `children` property is an array of children nodes. These can be elements, text, processing instructions, or comments. `XMLElement` declares an enumerated type, `Child`, which looks like:

	public enum Child {
		case Element(XMLElement)
		case Text(String)
		case ProcessingInstruction(target: String, value: String)
		case Comment(String)
	}

Let's change the text of `newAnchorElement` of the above example—discarding anything else the anchor element might contain—like this:

	newAnchorElement.children = [.Text("Click here")]

The `.Text` qualification can be dropped, so the above is equivalent to the following:

	newAnchorElement.children = ["Click here"]

Finally, the `childElements` property is a read-only version of `children` that only contains child nodes of type `XMLElement`. It ignores any text, processing instructions, and comments—which is handy for handling XML elements with no or barely any mixed content.

### Generating
To generate a serialisation of an XML element, use `XMLElement`'s `serialiseAsRootElementInDocument(preficesByNamespace:)` method. It takes a mapping of namespaces to prefices—all namespaces are declared on the root element.

The `serialiseAsRootElementInDocument(preficesByNamespace:)` method exists in an `NSData` and a `NSString` variant. As an example, let's serialise `rootElement` into a file in the current directory.

	do {
		try rootElement.serialiseAsRootElementInDocument(preficesByNamespace: [namespace: "h"]).writeToURL(NSURL.fileURLWithPath("file.xml"), atomically: true)
	} catch {
		print("Serialising failed.")
	}

### Building
One way to build or manipulate XML is by using the `attributes`, `children`, and `childElements` properties and constructing elements using the `XMLElement(type:attributes:children:)` initialiser. This might be sufficient for transformations, but it's cumbersome to create XML structures within code. Surf's domain–specific language comes to the rescue for exactly that problem—and it's type-safe.

![An HTML document using Surf's DSL](dsl.png "An HTML document using Surf's DSL")